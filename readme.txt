Comandos para usar GIT

Para empezar clonamos un repositorio 
git clone https://gitlab.com/vmoyano/ejemplogitp1.git

Consulta
git status

Agregar un archivo
git add (archivo)
git add .

Hacer commit agregando un mensaje con los cambios realizados
git commit -am "Mensaje completo y consiso"

Subir los cambios al servidor
git push origin master
git push

Bajar la ultima versión
git pull origin master
git pull